<?php

/**
 * 
 * websocket
 * 
 * 是html5开始提供的一种在单个TCP连接上进行全双工通讯的协议
 * （对讲机单工，电话双工）
 * 
 * websocket解决服务器端与客户端即时通信的问题
 * 
 * 协议名：ws  加密通信 wss 通信成功 状态码101
 * 
 * 浏览器支持
 * SOCKET.io
 * IE11.0开始支持
 * 
 * html5中websocketApi
 * //ws 协议名
 * //localhost 连接主机
 * //端口号
 * 
 * var ws = new webSocket("ws://localhost:9501");
 * 
 * 网络通信三要素：协议、主机、端口
 * 
 * 
 * swoole 实现websocket服务
 * 
 * WebSocket\Server基础自Http\Server
 * 
 * $server = new Swoole\WebSocket\Server("0.0.0.0", 9501);
 * //当websocket客户端与服务器建立连接并完成握手后会回调此函数
 * $server->on('open', function(Swoole\WebSocket\Server $server, Swoole\Http\Request $request){
 *      echo "已连接成功\n";
 *      //服务器端向客户端发送消息
 *      $server->push($request->fd, '欢迎进入我们的聊天室');
 * });
 * 
 * //服务器收到来自客户端的数据帧时会回调此函数 此回调方法不能缺少
 * $server->on('message', function(Swoole\WebSocket\Server $server, swoole_websocket_frame $frame){
 *      //服务端主动向客户端发送消息
 *      $frame->data 客户端发过来的数据
 *      //服务端向客户端发送消息
 *      $server->push($frame->fd, "this is server");
 * });
 * 
 * //客户端关闭连接时触发此回调函数
 * $server->on('close', function($ser, $fd){
 *  echo "client {$fd} closed\n";
 * })
 * 
 * //启动服务
 * $server->start();
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */