<?php

$server = new Swoole\WebSocket\Server("0.0.0.0", 6060);

$server->on('open', function(Swoole\WebSocket\Server $server, Swoole\Http\Request $request){
     $server->push($request->fd, '欢迎进入我们的聊天室');
});

//服务器收到来自客户端的数据帧时会回调此函数 此回调方法不能缺少
$server->on('message', function(Swoole\WebSocket\Server $server, swoole_websocket_frame $frame){

    //$server->connections 所有已经连接过的客户端
    $data = $frame->data;

    foreach ($server->connections as $client){
        $server->push($client, $data);
    }   
});

//客户端关闭连接时触发此回调函数
$server->on('close', function($ser, $fd){
 echo "client {$fd} closed\n";
});

//启动服务
$server->start();