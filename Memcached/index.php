<?php


/**
 * 
 * Memcached
 * 
 * Free & open source, high-performance, distributed memory object caching system, generic in nature, but intended for use in speeding up dynamic web applications by alleviating database load.
 * 
 * Memcached is an in-memory key-value store for small chunks of arbitrary data (strings, objects) from results of database calls, API calls, or page rendering.
 * 
 * Memcached is simple yet powerful. Its simple design promotes quick deployment, ease of development, and solves many problems facing large data caches. Its API is available for most popular languages.
 * 
 * 
 * 缺点：
 *  数据没有永久保存，不能宕机或断电也不能重启服务，它的信息全部在内存当中，一但完成上述操作，数据将全部丢失
 * 
 * 使用场景：
 *  不重要且还很小很碎的数据，如登录成功后的session信息就可以存放memcached。
 * memcahced服务作为缓存使用，所以一般项目中读取数据使用它可以，写入或修改数据，几乎不用它
 * 
 * 
 * 2、应用图解
 * 减少数据库访问，提高web速度
 * 实质：不用去请求读取mysql，减少mysql的并发量和读写量
 * 
 * 3、与mysql进行比较
 * （1）与mysql一样是一个软件服务，需要启动服务
 * （2）mysql里面的数据，是存储到磁盘里面的，memcached里面的数据是存储到内存里面
 * （3）mysql使用表结构来存储数据，而memcached里面数据的存储是键值对（key=>value）;
 * 
 * 4、memcached中的一些参数限制
 * key原则
 * memcached的key的长不超过250字节，value大小限制为1M，默认端口号为11211；
 * 
 */



/**
 * 
 * 
 * 服务安装与启动
 * 
 * yum search memcached|grep ^memcached
 * 
 * yum install -y memcached.x86_64 memcached-devel.x86_64
 * 
 * 或者源码安装
 * 
 * 编译工作要提前安装好
 * 安装依赖
 * centos安装依赖所用的命令
 * yum install -y gcc gcc-c++ automake autoconf make cmake libevent-devel.x86_64
 * 
 * wget http://www.memcached.org/files/memcached-1.6.10.tar.gz
 * 源码安装memcached
 * tar zxf memcached.tar.gz
 * 
 * ccd memcached
 * ./configure --prefix=/usr/local/memcached
 * 
 * make && make install
 * 
 * 
 * 
 * 启动
 * memcached -n 16m -p 11211 -d -u root
 * 
 * -m 启动16兆内存   一个原则，指定的内存大小比物理内存小
 * -p  默认端口11211 
 * -d  启动守护进程
 * -u   指定用户
 * -c   指定连接数（并发数）
 * -l   是监听的服务器IP地址  127.0.0。1   0.0.0.0谁都可以访问
 * 
 * netstat -tunpl|grep 11211
 * 
 * telnet 127.0.0.1 11211
 * 
 * pkill memcached
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */


/**
 * 
 * 常用命令
 * 
 * 1、获取数据
 * 获取存储在key（键）中的value（数据值），如果key不存在，则返回空
 * get key
 * 
 * 
 * 2、添加设置数据
 * //添加add  只能添加不存在的key或过期了的key，存在的key添加则报错
 * add key flags exptime bytes\n
 * value\n
 * 
 * //设置set  key存在则修改，不存在则添加
 * set key flags exptime bytes\n
 * value\n
 * 
 * 
 * 参数说明如下
 * key：结构中的key，用于查找缓存值
 * flags：客户机使用它存储关于键值对的额外信息（0｜1｜2）
 * exptime：在缓存中保存简直对的时间长度（以秒为单位，0表示永远）
 * 【时间长度（最长30天），时间戳（时间戳可以设置很久的时间）】
 * bytes：在缓存中存储的字节数
 * value：存储的值长度和bytes长度设置的一样的
 * 
 * 输出信息说明：
 * STORED：保存成功后输出
 * 
 * 
 * 
 * 3、自增和自减
 * incr与decr命令用于对已存在的key（键）的数字值进行自增或自减操作
 * incr与decr命令操作的数据必须是十进制的32位无符号整数
 * 
 * 自增
 * 统计
 * 
 * incr key increment_value
 * 
 * 使用自增或自减第一步，要用set或add创建一个key值，才可以使用
 * 
 * 4、删除
 * delete 命令用于删除已存在的key（键）
 * delete key
 * 
 * 清除所有  尽量不要去使用
 * flush_all
 * 
 * 
 * 穿透和雪崩
 * 
 * 5、查看状态
 * 用于返回统计信息例如PID（进程号）、版本号、连接数等
 * stats
 * 
 * pid:memcache服务器进程id
 * uptime：服务器已运行秒数
 * curr_connections：当前连接数量
 * cmd_get：get命令请求次数
 * cmd_set: set命令请求次数
 * 
 * get_hits:get命令命中次数
 * get_misses:get命令未命中次数
 * 
 * 命中率：get_hits/cmd_get   60%-90%
 * 
 * 
 * 如果实时性要求不高，可以通过延长缓存时间提高命中率
 * 
 * 
 */



/**
 * 
 * 
 * PHP操作memcached
 * 
 * 1、安装扩展 
 * （1）安装依赖
 * yum install -y libmemcached.x86_64 libmemcached-devel.x86_64
 * 
 * (2)安装memcached扩展 
 * wget http://pecl.php.net/get/memcached-3.1.3.tgz
 * 
 * tar zxvf memcached-3.1.3.tgz 
 * cd memcached-3.1.3
 * phpize命令  （在或者文件所解压目录中去执行） 生成configure文件
 * 注意：phpize命令是需要在yum安装php是一定要安装php版本-dev
 * 
 * which php-config
 * ./config --with-php-config=/usr/local/php/bin/php-config
 * 
 * make && make install
 * 
 * 配置php.ini
 * extension = memcached.so
 * 
 * 查看是否有memcached
 * php -m|grep memcached
 * 
 * netstat -tunpl
 * 
 * 如果是php+apache就需要重启apache服务就可以让其扩展生效
 * 如果是nginx+php-fpm就需要重启php-fpm
 * 
 * 
 * 
 * 2、图形工具管理memcached
 * memcached在PHP有一个开源的管理web工具，memadmin
 * 网址：http://www.junopen.com/memadmin
 * 
 * 下载对应源码包，解压到web目录中
 * 
 * 
 * 
 * 3、php操作memcache
 * 
 * 看conn.php文件
 * 
 * 
 * 4、集群
 * 
 * 修改php.ini，让其支持一致性hash的集群算法
 * 
 * memcache.hash_strategy = consistent
 * 
 * 配置完毕，重启web服务器或者php-fpm 
 * 
 * 5、session写入到memcached
 * 自定义session存储介质，默认session写入到服务器文件中
 * 使用php提供函数来修改
 * 
 * session_set_save_handler — 设置用户自定义会话存储函数
 * 
 * open(string $savePath, string $sessionName)
 * open 回调函数类似于类的构造函数， 在会话打开的时候会被调用
 * 
 * close()
 * close 回调函数类似于类的析构函数。 在 write 回调函数调用之后调用。
 * 
 * read(string $sessionId)
 * 如果会话中有数据，read 回调函数必须返回将会话数据编码（序列化）后的字符串
 * 
 * write(string $sessionId, string $data)
 * 在会话保存数据时会调用 write 回调函数。
 * 
 * 
 * 
 */














