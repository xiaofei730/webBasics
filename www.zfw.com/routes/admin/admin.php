<?php

//后台路由
// use App\Http\Controllers\Admin\LoginController;

//路由分组
Route::group(['prefix' => 'admin', 'namespace' => 'App\Http\Controllers\Admin'], function(){
    //登录显示 name给路由起一个别名
    // Route::get('login', [LoginController::class, 'index'])->name('admin.login');
    Route::get('login', 'LoginController@index')->name('admin.login');
    //登录处理
    Route::post('login', 'LoginController@login')->name('admin.login');

    //  后台需要验证才能
    Route::group(['middleware' => ['ckadmin']], function(){
        //后台首页显示
        Route::get('index', 'IndexController@index')->name('admin.index');

        //欢迎页面显示
        // Route::get('welcome', 'IndexController@welcome')->name('admin.welcome')->middleware(['ckadmin']);
        Route::get('welcome', 'IndexController@welcome')->name('admin.welcome');
        //退出
        Route::get('logout', 'IndexController@logout')->name('admin.logout');
    });
    

});