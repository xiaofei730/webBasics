<?php

namespace Database\Seeders;

//用户模型
use App\Models\User;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //清空--数据表
        User::truncate();
        //添加模拟数据 50用户
        User::factory()->count(50)->create();

        //规定id = 1用户名为admin
        User::where('id', 1)->update(['username' => 'admin']);
    }
}
