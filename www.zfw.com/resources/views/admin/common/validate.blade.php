<!-- 验证错误信息提示 -->
@if($errors->any())
    <div class="errorTip">
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>
@endif