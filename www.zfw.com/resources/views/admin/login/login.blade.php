<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台管理登录</title>
    <style type="text/css">
        *{
			margin: 0;
			padding: 0;
		}
        body{
			background-image: url(/images/admin/adminBg.jpeg);
            background-size: 100%;
		}
        .box{
			width: 310px;
			margin: 200px auto 0px;
			color: #fff;
			text-align: center;
		}
		.box p{
			line-height: 50px;
		}
		.box p input{
			background-color: transparent;
			border: none;
			outline: none;
			padding-left: 10px;
			color: #fff;
			font-size: 16px;
		}
		.forgetPassword{
			text-align: right;
		}
		.forgetPassword a{
			color: #fff;
			text-decoration: none;
			font-size: 14px;
		}
		.submit{
			width: 224px;
			height: 42px;
			font-size: 14px;
			border: none;
			outline: none;
			cursor: pointer;
			color: #fff;
            background-color: blue;
		}
        .errorTip{
            width: 200px;
            height: 50px;
			margin: 200px auto 0px;
			color: red;
			text-align: center;
            background-color: orange;
        }

    </style>
</head>
<body>
    <div class="box">
        <!-- 引入验证-->
        @include('admin.common.validate')
        <!-- 消息提示 -->
        @include('admin.common.msg')
		<form action="{{ route('admin.login')}}" method="post">
            @csrf
			<p>姓名：<input type="text" name="username"></p>
			<p>密码：<input type="password" name="password"></p>
			<p class="forgetPassword"><a href="">忘记密码?</a></p>
			<input type="submit" value="登录" class="submit">
		</form>
	</div>
</body>
</html>

