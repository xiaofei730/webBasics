<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware(['ckadmin:login']);
    }

    //登录显示
    public function index() 
    {
        if (auth()->check()) {
            return redirect(route('admin.index'));
        }
        return view('admin.login.login');
    }

    public function login(Request $request)
    {
        //表单验证
        $post = $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ],[
            'username.required' => '账户都不写，想飞吗？'
        ]);
        // dump($post);
        // dump($request->all());

        //登录
        // auth()->guard();
        //注：账号和密码的字段名一定要用username 和password
        $bool = auth()->attempt($post);
        //判断是否登录成功
        if ($bool) {
           //登录成功 
           //auth()->user() 返回当前登录的用户模型对象，存储在session
           //laravel默认session是存储在文件中，优化到memcached  redis
           $model =  auth()->user();
            dump($model->toArray());
            //跳转到后台页面

            return redirect(route('admin.index'));
        }

        //withErrors 把信息写入到验证错误提示中   特殊的session laravel中叫  闪存
        //闪存  从设置好之后，只能在第一个http请求中获取到数据，以后就没有了
        return redirect(route('admin.login'))->withErrors(['error' => '登录失败']);
    }
}
