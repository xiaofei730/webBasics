<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    //后台首页显示
    public function index()
    {
        return view('admin.index.index');
    }

    //欢迎页面
    public function welcome()
    {
        return view('admin.index.welcome');
    }

    public function logout()
    {
        //用户退出，清空session
        auth()->logout();

        //跳转 带提示   闪存  session
        return redirect(reoute('admin.login'))->with('success', '请重新登录');
    }
}
